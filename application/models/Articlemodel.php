<?php
class Articlemodel extends CI_Model {
    public function articles_list($limit,$offset)
   {
         $user_id = $this->session->userdata('user_id');
         $query = $this->db
                       ->select()
                       ->from('articles')
                       ->where(['user_id'=> $user_id])
                       ->limit($limit,$offset)
                       ->get();    
          return $query->result();   
   }

   public function num_rows()
   {
   $user_id = $this->session->userdata('user_id');
   $query = $this->db
                  ->select()
                  ->from('articles')
                  ->where(['user_id'=> $user_id])
                  ->get();
     return $query->num_rows();
   }

    public function add_article($post)
    {
      return $this->db->INSERT('articles', $post);
    }

    public function add_category($post)
    {
      $this->db->insert('category',$post);
      
    }


    public function category()
    {
      $query = $this->db->get('category');      
          return $query->result();  
   }


   /* public function myjoin()
    {
        $this->db->select('*');
        $this->db->from('articles');
        $this->db->join('category', 'articles.id = category.c_id');
        return $this->db->get();
        
      
      /*SELECT articles.id,articles.title,articles.body,articles.user_id,category.cat_1 FROM articles 
      INNER JOIN category ON articles.cat_1 = category.c_id;
    }*/


 


    public function search_article($id)
    {    
       $r = $this->db->SELECT(['id','title','body'])
                     ->WHERE('id',$id)
                     ->get('articles');
            return $r->row();
    }

    public function update_article($id, $article)
    {
      return $this->db->WHERE('id',$id)
                      ->UPDATE('articles',$article);
    }

    public function delete_article($id)
    {
       return $this->db->delete('articles',['id'=>$id]);
    }

  
}
?>