<?php
if(!isset($_SESSION["u_id"]))
{
  $this->session->set_flashdata('login_failed','please enter Username & Password');
  return redirect('Userlogin');  
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Articles List</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('tools/css/bootstrap.css');  ?>"> 
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand">Article List</a>
    <?php echo anchor("Userlogin/logout","Logout",['class'=>'btn btn-dark rounded-circle', ]); ?>
  </div>
</nav>
<br>
<div class="container">

<table class="table">
    <thead>
      <th>TITLE</th>
      <th>BODY</th>
      <th>USER_ID</th>
      <th>CATEGORY</th>
    </thead>
<?php  foreach ($query->result() as $row1 ) ?>
  <tr>
  <td><?php echo $row1->title; ?></td>
  <td><?php echo $row1->body; ?></td>
  <td><?php echo $row1->user_id; ?></td>
  <td><?php echo $row1->cat; ?></td>
  </tr>
</table>
</div>
