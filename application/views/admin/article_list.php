<?php
if(!isset($_SESSION["user_id"]))
{
  $this->session->set_flashdata('login_failed','please enter Username & Password');
  return redirect('login');   
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ARTICLE LIST</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('tools/css/bootstrap.css');  ?>"> 
    <link rel="stylesheet" type="text/css" href="<?= base_url('tools/css/custom.css');  ?>"> 
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">ARTICLE LIST</a>
    <?php echo anchor("Login/logout","LOGOUT",['class'=>'btn btn-dark']); ?>
  </div>
</nav>
<div class="container">
  <br>
  <?php if( $feedback = $this->session->flashdata('feedback') ): ?>
    <div class="row">
      <div class="col-lg-6">
        <div class="alert alert-dismissible alert-danger"> 
          <?= $feedback ?>
        </div>
      </div>
    </div>
      <?php endif; ?>
      <?php if( $msg = $this->session->flashdata('msg') ): ?>
    <div class="row">
      <div class="col-lg-6">
        <div class="alert alert-dismissible alert-danger"> 
          <?= $msg ?>
        </div>
      </div>
    </div>
      <?php endif; ?>
  <div class="row">
    <div class="col-lg-6">
    <?php echo anchor("admin/validation","ADD ARTICLE",['class'=>'btn btn-success']); ?>
    </div>
  </div>
  <br>
  <table class="table">
    <thead>
      <th>ID</th>
      <th>ARTICLE TITLE</th>
      <th>EDIT</th>
      <th>DELETE</th>
    </thead>
    <tbody>
      <?php if(count($articles)): ?>
      <?php foreach ($articles as $art): ?>
      <tr>
        <td><?php echo $art->id; ?></td>
        <td><?php echo $art->title; ?></td>
        <td><?= anchor("admin/edit_article/{$art->id}",'EDIT',['class'=>'btn btn-secondary']);?></td>
        <td>
          <?=
            form_open('admin/delete_article'),
            form_hidden('id',$art->id),
            form_submit(['name'=>'submit','value'=>'DELETE','class'=>'btn btn-danger']),
            form_close();
          ?>
        </td>
      </tr>
      <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
  <?= $this->pagination->create_links(); ?>
</div>
<?php include('admin_footer.php'); ?>
