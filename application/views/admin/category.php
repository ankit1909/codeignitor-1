<?php
if(!isset($_SESSION["user_id"]))
{
  $this->session->set_flashdata('login_failed','please enter Username & Password');
  return redirect('login');   
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add category</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('tools/css/bootstrap.css');  ?>"> 
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand">Add category</a>
    <?php echo anchor("Login/logout","Logout",['class'=>'btn btn-dark']); ?>
  </div>
</nav>
<div class="container">
  <?php echo form_open('Admin/validation');?>

    <legend>Add Category</legend>
    
    <div class="form-group">
      <label>Category</label>
      <input type="text" name="cat" class="form-control" />
      <span class="text-danger"><?php echo form_error('cat'); ?></span>
    </div>

    <button type="submit" name="insert" class="btn btn-primary">Submit</button>
    <?php echo form_close() ?>
</div>

<?php include_once('admin_footer.php'); ?>