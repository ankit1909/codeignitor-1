<?php include('public_header.php'); ?>

<div class="container">
  <?php echo form_open('login/admin_login'); ?>
    <fieldset>
      <legend>Admin Login</legend>
      <?php if( $error = $this->session->flashdata('login_failed') ): ?>
      <div class="row">
        <div class="col-lg-6">
          <div class="alert alert-dismissible alert-danger"> 
            <?= $error ?>
          </div>
        </div>
      </div>
      <?php endif; ?>
    
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <label for="username">User Name</label>
            <input type="text" class="form-control" name="username" id="Email" placeholder="username">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <label for="InputPassword">Password</label>
            <input type="password" class="form-control" name="password" id="Password" placeholder="Password">
          </div>
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </fieldset>
  </form>
</div>
<div class="col-lg-6">
  <?php echo validation_errors(); ?>
</div>
<?php include('public_footer.php'); ?>