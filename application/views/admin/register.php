<?php include_once('admin_header.php'); ?>
<?php if($msg = $this->session->flashdata('message')):  ?>

<div class="row">
    <div class="col-md-">
        <div class="alert alert-dismissible alert-success">
            <?php echo $msg; ?>
        </div>
    </div>
    <?php endif; ?>
</div>
<div class="container">
    <?php echo form_open("admin/adminsignup", ['class'=>'form-horizontal']);   ?>
    <h3><strong>Admin Registration</strong></h3>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Username</label>
                    <div class="col-md-9">
                        <?php echo form_input(['name'=>'username', 'class'=>'form-control','placeholder'=>'username']); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Email</label>
                    <div class="col-md-9">
                        <?php echo form_input(['name'=>'email', 'class'=>'form-control','placeholder'=>'email']); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Password</label>
                    <div class="col-md-9">
                        <?php echo form_password(['name'=>'pass', 'class'=>'form-control','placeholder'=>'password']); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
        </div>
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
    <?php echo anchor("Admin/adminsignup", "BACK", ['class'=>'btn btn-primary']); ?>
    <?php echo form_close();  ?>
</div>
<div class="col-lg-6">
    <?php echo validation_errors(); ?>
</div>
<?php include_once('admin_footer.php'); ?>

