<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Articles List</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('tools/css/bootstrap.css');  ?>"> 
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
<div class="container-fluid">
  <a class="navbar-brand" href="#">Dashboard</a>
  <form class="form-inline my-4 my-lg-0">
    <?php echo anchor("admin/adminsignup","REGISTER",['class'=>'btn btn-dark rounded-pill']); ?> 
    <br>
    <br>
    <?php echo anchor("userlogin","USER LOGIN",['class'=>'btn btn-dark rounded-pill']); ?> 
  </form>
</div>
</nav>