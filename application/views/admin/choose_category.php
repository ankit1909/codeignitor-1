<?php
if(!isset($_SESSION["user_id"]))
{
  $this->session->set_flashdata('login_failed','please enter Username & Password');
  return redirect('login');   
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Articles List</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('tools/css/bootstrap.css');  ?>"> 
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand">Article List</a>
    <?php echo anchor("Login/logout","Logout",['class'=>'btn btn-dark']); ?>
  </div>
</nav>

<div class="container">

<br><br>
<?php echo form_open('Admin/store_article'); ?>
<?php echo form_hidden('user_id', $this->session->userdata('user_id')); ?>
<fieldset>
<label for="InputCat">Choose Category</label>
<select name="cat">
<?php
if(count($fetch_data) )
{
    foreach($fetch_data as $row1)
    {
    ?>
        <option value="<?php echo $row1->c_id; ?>"><?php echo $row1->Cat; ?></option>
    <?php
    }
}
?>
</select>
<div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label for="InputTitle">Article Title</label>
          <?php echo form_input(['name'=>'title', 'class'=>'form-control','placeholder'=>'artical title']); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label for="InputBody">Article Body</label>
          <?php echo form_textarea(['name'=>'body', 'class'=>'form-control','placeholder'=>'artical body']); ?>
        </div>
      </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </fieldset>
  </form>
</div>
<div class="col-lg-6">
  <?php echo validation_errors(); ?>
</div>
<?php include_once('admin_footer.php'); ?>






















  