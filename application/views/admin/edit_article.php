<?php
if(!isset($_SESSION["user_id"]))
{
  $this->session->set_flashdata('login_failed','please enter Username & Password');
    return redirect('login');   
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Articles List</title>
  <link rel="stylesheet" type="text/css" href="<?= base_url('tools/css/bootstrap.css');  ?>"> 
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand">Update Article</a>
    <?php echo anchor("Login/logout","Logout",['class'=>'btn btn-dark']); ?>
  </div>
</nav>
<div class="container">
  <?php echo form_open("admin/update_article/{$article->id}",['class'=>'form-horizontal']);?>
  <fieldset>
    <legend>Edit Articles</legend>
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <label for="InputEmail">Article Title</label>
            <?php echo form_input(['name'=>'title', 'class'=>'form-control','placeholder'=>'article title','value'=>set_value('title',$article->title)]); ?>
          </div>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="form-group">
          <label for="InputPassword">Article Body</label>
          <?php echo form_textarea(['name'=>'body', 'class'=>'form-control','placeholder'=>'article body','value'=>set_value('body',$article->body)]); ?>
        </div>
      </div>
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
  </fieldset>
</div>
</form>
<div class="col-lg-6">
  <?php echo validation_errors(); ?>
</div>
<?php include_once('admin_footer.php'); ?>