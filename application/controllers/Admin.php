<?php
class Admin extends MY_Controller
{
    public function dashboard()
    {
      $this->load->model('Articlemodel','ar');
    $this->load->library('pagination');  
$config = [
    'base_url' => base_url('index.php/Admin/dashboard'),
    'per_page' => 3,
    'total_rows' => $this->ar->num_rows(),
    'full_tag_open' => "<ul class='pagination'>",
    'full_tag_close' => "</ul>",
    'next_tag_open' => '<li>',
    'next_tag_close' => '</li>',
    'prev_tag_open' => '<li>',
    'prev_tag_close' => '</li>',
    'num_tag_open' => '<li>',
    'num_tag_close' => '</li>',
    'cur_tag_open' => "<li class='active'><a>",
    'cur_tag_close' => '</a></li>',
];    
    $this->pagination->initialize($config);
      $articles=$this->ar->articles_list($config['per_page'],$this->uri->segment(3));
      $this->load->view('admin/article_list',['articles'=>$articles]);
    }

    public function adminsignup()
    { 
      $this->form_validation->set_rules('username','User Name','required|trim');
      $this->form_validation->set_rules('email','Email','required|valid_email');
      $this->form_validation->set_rules('password','password');

      $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
      if( $this->form_validation->run() ){
      $this->load->library('encryption');

      $data = array(
              'username' => $this->input->post('username'),
              'email' => $this->input->post('email'),
              'password' => $this->encryption->encrypt($this->input->post('password')),
      );
      $this->load->model('queries');
      if( $this->queries->registeradmin($data)){
        $this->session->set_flashdata('message', 'admin Register successfully');
      return redirect("Admin/adminsignup");
      }else{
        $this->session->set_flashdata('message', 'Failed to register');
      return redirect("Admin/adminsignup");
      }
    }else{
        $this->load->view('admin/register');
      }
    }

    public function store_article(){
      $this->form_validation->set_rules('title','article title','required');
      $this->form_validation->set_rules('body','article body','required');
      $this->form_validation->set_rules('cat','Category');
      $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
      if( $this->form_validation->run() ){
        $post = array(
          'title' => $this->input->post('title'),
          'body' => $this->input->post('body'),
          'cat' => $this->input->post('cat')
  );

       /* $post=$this->input->post();*/
        $this->load->model('articlemodel');
      if( $this->articlemodel->add_article($post) ){
        $this->session->set_flashdata('feedback','**Article added successfully**');
      }else{
        $this->session->set_flashdata('feedback','**Article failed to add, please try again**');
      }
      return redirect('Admin/dashboard');
      }else{
        $this->load->view('admin/add_article');
      }  
  }

public function validation(){
 $this->form_validation->set_rules('cat','category','required');
 if($this->form_validation->run() )
 {
   $this->load->model('Articlemodel');
   $post = array(
              "cat" =>$this->input->post("cat")
   );
   $this->Articlemodel->add_category($post);
    return redirect('Admin/fetch_category');
 }
 else
 {
  $this->load->view('admin/category');
 }
}

public function fetch_category(){
  
  $this->load->model('Articlemodel');
  $data["fetch_data"]= $this->Articlemodel->category();
  $this->load->view('admin/choose_category',$data);
}

public function view_article(){

      $this->load->model('Articlemodel');
      $data['query'] = $this->Articlemodel->myjoin();
      $this->load->view('user/view_article',$data);
    }

public function edit_article($id)
    {
      $this->load->model('articlemodel','rock');
      $article = $this->rock->search_article($id);
      $this->load->view('admin/edit_article',['article'=>$article]); 
    }

public function update_article($id)
    {
      $this->form_validation->set_rules('title','article title','required|alpha');
      $this->form_validation->set_rules('body','article body','required');
      $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");
      if( $this->form_validation->run() ){
      $post=$this->input->post();
      $this->load->model('articlemodel','bb');
      if( $this->bb->update_article($id,$post) ){
        $this->session->set_flashdata('feedback','**Article updated successfully**');
      }else{
        $this->session->set_flashdata('feedback','**Article failed to update, please try again**');
      }
        return redirect('Admin/dashboard');
    } else{
      $this->load->view('admin/add_article');
    }
  }
  
    public function delete_article()
    {
      $id=$this->input->post('id');
      $this->load->model('articlemodel','deletearticle');
      if($this->deletearticle->delete_article($id))
      {
        $this->session->set_flashdata('msg','article delete successfully');
      }else{
        $this->session->set_flashdata('msg','article not delete please try again');
      }
        return redirect('admin/dashboard');
    }    
}
?>