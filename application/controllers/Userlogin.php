<?php
class Userlogin extends MY_Controller
{
    public function index()
    {
        $this->load->view('user/userlogin'); 
    }

    public function user_login()
    {
        $this->form_validation->set_rules('username','User Name','required');
        $this->form_validation->set_rules('password','Password','required');


        if( $this->form_validation->run() ){

                $user = $this->input->post('username');
                $pass = $this->input->post('password');
                $this->load->model('User');
                $login_id = $this->User->v_login($user,$pass);
                if($login_id){
                    $this->load->library('session');

                    $this->session->set_userdata('u_id', $login_id);
                    //valid, login user
                    
                    return redirect('Admin/fetch_article');
                }else{
                    //failed login
                    $this->session->set_flashdata('login_failed','invalid username/password');
                   return redirect('Userlogin');
                }
        }else{
            //failed
            $this->load->view('user/view_article'); 
        }
                
    }
             

    public function logout()
    {
        $this->session->unset_userdata('u_id');
        return redirect("Userlogin");
    }
}
?>